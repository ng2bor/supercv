import { SupercvService } from './../supercv.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-contact',
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

	constructor(private _service: SupercvService) { }
	user = {}

	userEmail = ''
	userMessage = ''
	error = false
	errorType = 'danger'
	errorMsg = 'Error, input all filds'

	ngOnInit() {
		this.user = this._service.getUser()
	}

	doSend() {
		var data = {
			email: '',
			msg: '',
			subject: 'Contact from SuperCV',
			reciver: 'me@miff.me'
		}

		if ( this.mailFormat(this.userEmail) && this.userMessage.length > 10) {
			data.email = this.userEmail
			data.msg = this.userMessage
			let respond = this._service.fakeSend(data, 'http://localhost/send-email')
			if (respond) {
				//Hvala
				this.error = true
				this.errorType = 'success'
				this.errorMsg = 'Well done'
				this.userEmail = ''
				this.userMessage = ''
			} else {
				//GRESKA
				this.error = true
				this.errorType = 'danger'
				this.errorMsg = 'Error, server'
			}
		} else {
			// GRESKA neka
			this.error = true
			this.errorType = 'danger'
			this.errorMsg = 'Error, input all filds'
		}
	}

	mailFormat(email): boolean {

        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if (email != "" && (email.length <= 5 || !EMAIL_REGEXP.test(email))) {
            return false;
        }

        return true;
    }
}
