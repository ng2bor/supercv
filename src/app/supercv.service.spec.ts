import { TestBed, inject } from '@angular/core/testing';

import { SupercvService } from './supercv.service';

describe('SupercvService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupercvService]
    });
  });

  it('should ...', inject([SupercvService], (service: SupercvService) => {
    expect(service).toBeTruthy();
  }));
});
