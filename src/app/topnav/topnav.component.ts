import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"

@Component({
	selector: 'app-topnav',
	templateUrl: './topnav.component.html',
	styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {

	constructor(private _routes: Router) { }

	isNavShown = false
	bgrcolor = ''

	ngOnInit() {
		console.log(this._routes.url)
		let url = this._routes.url
		this.bgrcolor = 'bg-' + url.substr(1)
	}

	// toggleNav() {
	// 	this.isNavShown = !this.isNavShown
	// }

}
