import { Injectable } from '@angular/core';

@Injectable()
export class SupercvService {

	constructor() { }

	fakeSend(data: any, url: String): Boolean {
		// TODO Posalji {data} na {url}
		console.log('Poslato', data)
		return true
	}

	getUser() {
		var user = {
			'first_name': 'Milan',
			'last_name': 'Djordjevic',
			'dob': {
				year: {
					number: 1978, 
					str: '`78'
				},
				month: {
					number: 1, 
					str: 'Januar'
				},
				day: {
					number: 12, 
					str: '12'
				}
			},
			'avatar': 'https://en.gravatar.com/userimage/10744355/5d363288cd901dcbd122d6e01bf7a439.jpeg',
			'email': 'milan.miff@icloud.com',
			'twitter': 'miff78',
			'linkedin': 'miff78',
			'skills': ['Swift', 'NG2', 'Python', 'PHP', 'CSS3'],
			'education': '',
			'isEmployee': true
		}
		return user
	}

}
