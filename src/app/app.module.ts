import { SupercvService } from './supercv.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { TopnavComponent } from './topnav/topnav.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { SkillsComponent } from './skills/skills.component';
import { ContactComponent } from './contact/contact.component';

const appRoutes = [
	{
		path: 'home',
		component: HomeComponent
	},
	{
		path: 'profile',
		component: ProfileComponent
	},
	{
		path: 'contact',
		component: ContactComponent
	},
	{
		path: 'skills',
		component: SkillsComponent
	},
	{
		path: '**',
		component: HomeComponent
	}
]

@NgModule({
	declarations: [
		AppComponent,
		TopnavComponent,
		FooterComponent,
		HomeComponent,
		ProfileComponent,
		SkillsComponent,
		ContactComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [SupercvService],
	bootstrap: [AppComponent]
})
export class AppModule { }
