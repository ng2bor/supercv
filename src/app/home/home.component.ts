import { SupercvService } from './../supercv.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	constructor(private _service: SupercvService) { }
	user = {}
	

	ngOnInit() {
		this.user = this._service.getUser()
		console.log('Nesto tu', this.user)
	}

}
