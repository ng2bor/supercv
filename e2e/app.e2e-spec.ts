import { SuperCVPage } from './app.po';

describe('super-cv App', () => {
  let page: SuperCVPage;

  beforeEach(() => {
    page = new SuperCVPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
